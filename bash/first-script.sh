normal="$(tput sgr0)"
red="$(tput setaf 1)"
green="$(tput setaf 2)"
magenta="$(tput setaf 5)"

filesDiff=$( diff <(ls dir/) <(ls dir0/) )

if [[ -n $filesDiff ]]; then
  echo
  echo "${red}WARNING: There is a difference in the files present in the two directories.${normal}"
  echo
  echo
  echo -n "${magenta}pausing for dramatic effect."
  sleep 1
  echo -n "."
  sleep 1
  echo ".${normal}"
  sleep 1
  echo
  echo
  echo "$filesDiff"
  echo
  echo
  read -n 1 -t 3 -s -r -p "Press any key to continue"
else
  echo
  echo "${green}The files present in the two directories are synced${normal}"
  sleep 1
  echo
fi

fileContentsDiff=$( diff dir/ dir0/ )

if [[ -n $fileContentsDiff ]]; then
  echo
  echo "${red}WARNING: There is a difference in the contents of the files present in the two directories.${normal}"
  echo
  echo
  echo -n "${magenta}pausing for dramatic effect."
  sleep 1
  echo -n "."
  sleep 1
  echo ".${normal}"
  sleep 1
  echo
  echo
  echo "$fileContentsDiff"
  echo
  echo
  read -n 1 -t 3 -s -r -p "Press any key to continue"
else
  echo
  echo "${green}The contents of the files present in the two directories are synced${normal}"
  sleep 1
  echo
fi
